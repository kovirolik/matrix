<?php
require 'conn.php';
 ?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://unpkg.com/purecss@0.6.0/build/pure-min.css">
  <link rel="stylesheet" href="app.css">
  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
  <script src="https://cdn.jsdelivr.net/g/pace@1.0.2,zepto@1.2.0"></script>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <script src="https://cdn.rawgit.com/Pagawa/PgwModal/master/pgwmodal.min.js"></script>
  <link rel="stylesheet" href="https://cdn.rawgit.com/Pagawa/PgwModal/master/pgwmodal.min.css">
  <script>
  $('.main-carousel').flickity({
    // options
    cellAlign: 'left',
    contain: true
  });</script>
  <title>Matrix</title>
</head>
<body>
  <div class="pure-menu pure-menu-horizontal nav">
            <a href="/" class="pure-menu-heading pure-menu-link">Matrix</a>
            <ul class="pure-menu-list">
                <li class="pure-menu-item"><a href="/panel" class="pure-menu-link">ПУ</a></li>
                <li class="pure-menu-item"><a href="https://forum.mtrxwrld.us" class="pure-menu-link">ПУ</a></li>
                <li class="pure-menu-item"><a href="https://wiki.mtrxwrld.us" class="pure-menu-link">Wiki</a></li>
                <li class="pure-menu-item" ><a href="javascript:login()"  style="float:right" class="pure-menu-link">Войти</a></li>
            </ul>
        </div>
      <h1>Новости</h1>
      <div class="main-carousel">
<?php
$conn = new mysqli("localhost", "root", "", "matrix");
$res = mysqli_query($conn, "SELECT * from news");
while ($row = $res->fetch_assoc()) {
  echo '<div class="carousel-cell"><a href="'.$row['requid'].'><img src="'.$row['imgurl'].'"></a></div>';
}
 ?></div>
   <script>
        function login() {
        $.pgwModal({
    content: '<h1>Войти в аккаунт</h1><form  class="pure-form pure-form-stacked" action="post" method="login.php"><fieldset><label for="login">Логин</label><input id="email" type="email" placeholder="Введите ваш логин"><label for="password">Парооль</label><input id="password" type="password" name="password" placeholder="Ваш пароль"><button type="submit" class="pure-button pure-button-primary">Войти <i class="fa fa-sign-in" aria-hidden="true"></i></button></fieldset></form>'
    });
  }
</script>
</body>
</html>
